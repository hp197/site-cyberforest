// https://bootstrap-vue.js.org/docs/reference/images
const publicPaths = {
  pages: '/site-cyberforest/',
  production: 'https://mch2022.org/',
  default: '/',
};
const env = process.env.NODE_ENV ?? 'default';

module.exports = {
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options.transformAssetUrls = {
          img: 'src',
          image: 'xlink:href',
          'b-img': 'src',
          'b-img-lazy': ['src', 'blank-src'],
          'b-card': 'img-src',
          'b-card-img': 'src',
          'b-card-img-lazy': ['src', 'blank-src'],
          'b-carousel-slide': 'img-src',
          'b-embed': 'src'
        };

        return options
      })
  },
  publicPath: env in publicPaths ? publicPaths[env] : publicPaths['default'],
  devServer: {
    proxy: 'https://mch2021.org'
  }
};
